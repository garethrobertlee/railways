import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators.js';

import Main from './Main.js';


function mapStateToProps(state){
	console.log(state)
	return {
		region: state.region
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
  }, dispatch);
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main);

export default App;