import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { bindActionCreators } from 'redux';
import styles from './Mapping.css';

import Mapping from './Mapping.js';
import Charting from './Charting.js';

const classNames = require('classnames');

class Button extends Component {

    constructor(){
        super()
       
    }

    render(){
        return(
            <div style={{display:'flex'}}>
               <button onClick={() => this.props.showBusy(900000)}>show busy on map</button>
               <button onClick={() => this.props.showAll()}>show all on map</button>
               <button onClick={() => this.props.showIncreaseChart()}>show Increase Chart</button>
                <button onClick={() => this.props.showTotalsChart()}>show 13/14 chert</button>
            </div>
        )
    }
}
export default Button