import React, { Component } from 'react';

import Mapping from './Mapping.js';
import Charting from './Charting.js';

class Contains extends Component {

    render(){
        return(
            <div style={{display:'flex'}}>              
                <Mapping {...this.props}/>           
            </div>
        )
    }
}
export default Contains