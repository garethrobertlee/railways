import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { bindActionCreators } from 'redux';
import styles from './Charting.css';
import moment from 'moment';

import { connect } from 'react-redux';

const classNames = require('classnames');

  class Charting extends Component {

  constructor(props){
    super()
    this.stations = this.stations.bind(this)
  }

  stations(){

    if (this.props.stations){  
      const topTen = this.props.stations.slice(0,10)
       this.chart(topTen.reverse())
    }
  }

  chart(topTen){

    d3.select('#graphic').selectAll('svg').remove();
    let margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 560 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    // set the ranges
    let y = d3.scaleBand()
      .range([height, 0])
      .padding(0.1);

    let x = d3.scaleLinear()
      .range([0, width]);
          
    let svg = d3.select("#graphic").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("class", styles.padit)
      .append("g")
      .attr("class", styles.padit)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    let self = this

    this.props.stations.forEach(function(d) {
      d[self.props.indicator] = +d[self.props.indicator];
    });
 
    x.domain([0, d3.max(topTen, function(d){ return d[self.props.indicator]/1000; })])
    y.domain(topTen.map(function(d) { return d['Station Name']; }));


    // append the rectangles for the bar chart
    svg.selectAll(".bar")
      .data(topTen)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("fill", function(d){
        if (d[self.props.indicator] > 900000){
          return "tomato"
        }
      })

      .attr("y", function(d) { return y(d['Station Name']); })
      .attr("height", y.bandwidth())
      .attr("width", function(d) {return x(d[self.props.indicator]/1000); } )
      .transition()
			.duration(200)
			.delay(function (d, i) {
				return i * 50;
			})

    // add the x Axis
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

    // add the y Axis
    svg.append("g")
      .call(d3.axisLeft(y));
  }

  render(){
    return(<div>{this.props.indicator}<div id="graphic"></div><ul style={{'listStyle':'none'}}>{this.stations()}</ul></div>)
  }
}

export default Charting;
