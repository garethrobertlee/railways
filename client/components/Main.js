import React, { Component } from 'react';

class Main extends Component {

  constructor() {
    super();
   }

  render() {
    const container = {
       width:'100%',
       maxWidth:'1320px',
       display: 'flex',
       flexDirection: 'column',
       height:'100vh'
    }

    return (
      <div> 
        <div style={container}>
          <div style={{'width':'320px'}}> VERY much a work in progress! It shows rail usage by station in 2014, and percentage increase since 1997.
                Can view entire West Yorkshire region or any of the 5 boroughs</div>
          {React.cloneElement(this.props.children, this.props)}
          </div>
      </div>
    );
  }
}

export default Main;


