import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import styles from './Mapping.css';
import css from './more.css';
import Button from './Button';

import Charting from './Charting.js';

const classNames = require('classnames');
 
class Mapping extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleStationClick = this.handleStationClick.bind(this);
    this.showBusy = this.showBusy.bind(this)
    this.showAll = this.showAll.bind(this)
    this.showIncreaseChart = this.showIncreaseChart.bind(this);
    this.showTotalsChart = this.showTotalsChart.bind(this);
    this.fillInData = this.fillInData.bind(this)
    this.state = {
      chartName: "Total Usage"
    }
  }
 
  helperFunction(identifier){
 
    const regions = {
      "E08000035":"Leeds",
      "E08000036":"Wakefield",
      "E08000034":"Kirklees",
      "E08000032":"Bradford",
      "E08000033":"Calderdale"
    }
    console.log(35)
    this.setState({
      district:regions[identifier],
      identifier: identifier,
    })
    this.updateStations(identifier)
  }

  componentDidMount() {
 
    this.makingAMap(this.props.region);
    this.gettingThoseStats(this.props.region)
  }

  componentWillReceiveProps(nextProps) {
  
    if (nextProps.region != this.props.region){   
      this.makingAMap(nextProps.region);
    }
  }

  updateStations(identifier){
    const indicator = this.state.indicator;
     d3.selectAll('circle').attr('opacity','0.3');
     d3.selectAll('circle').attr('fill','#fff').attr('r','6px').attr('stroke','none');
   
     let newStations = this.state.allStations.filter((f) =>
        f['District or Unitary Authority'] === this.state.district 
        ).sort(function(a, b){return b[indicator] - a[indicator]});
    
      newStations.map(station => {
        d3.select(`#${station['Origin TLC']}`).attr('opacity', 1)
      });
    
      this.setState({
        stations:newStations,
        indicator: indicator
      });

      if (this.state.showBusy){
        this.fillInData(this.state.stations, this.state.amount)
      };
  }

  bigIncreases(stations){

    stations.map(station => {
      station.increase = ((station['1314 Entries & Exits'] / station['0405'])-1)*100
    })
  }

  showTotalsChart(){
    const newStations = this.state.stations
    .sort(function(a, b){return b['1314 Entries & Exits'] - a['1314 Entries & Exits']})
    .filter(station =>  station.increase > 0)
    this.setState({
      stations:newStations,
      indicator: '1314 Entries & Exits'
    })
  }


  showIncreaseChart(){
    const newStations = this.state.stations
    .sort(function(a, b){return b['increase'] - a['increase']})
    .filter(function(station){return station.increase > 0})
    this.setState({
      stations:newStations,
      indicator: 'increase'
    })
  }

  fillInData(stations,amount){

    let station = "" 
    const newStations = stations.filter(station => {
      return station['1314 Entries & Exits'] >  amount
    });
     this.setState({
       amount:amount
    });

    newStations.map(function(f){
      station = f['Origin TLC']
      d3.select(`#${station}`).attr('fill','tomato').attr('r','10px').attr('stroke','black')
    });
     this.setState({
       amount:amount
    });

    this.setState({
      stations:newStations,
      indicator: '1314 Entries & Exits',
      showBusy:true,
      amount:amount,
    });  
  }

  showBusy(amount){
      this.fillInData(this.state.stations,amount)
  }


  renderingStations(svg,projection,locs,path){
    const url = `data/orig.json`;

    d3.json((url), (error, geodata) => {
      svg.selectAll('thiscanbeanything')
      .data(geodata.locations).enter()
      .append('circle')
      .attr('id', function(d){
        return d.crs       
      })
      .attr('cx', d => projection([d.lon, d.lat])[0])
      .attr('cy', d => projection([d.lon, d.lat])[1])
      .attr('r', '6px')
      .on('click', (d, i) => self.handleStationClick(d, identifier,projection,svg,i))
      .attr('fill', '#fff');         
    })
  }



  renderingLine(svg,projection,locs,path){ //not currently used, havent decided what to do with this
    const url = `data/west-yorkshire-railways-rail.geojson`;
   
    d3.json((url), (error, geodata) => {
      let path_data, track
      geodata.features.map(function(tracu){
        path_data = [{"type":"Feature",
          "id":tracu.properties.osm_id,
          "properties":{"name":"Linestring"},
          "geometry":{"type":"LineString",
          "coordinates": tracu.geometry.coordinates }
        }];
        track = svg.append("svg:g").attr("id", tracu.properties.osm_id);
        track.selectAll("path")
        .data(path_data)
        .enter().append("path")
        .attr("d", path).attr("class", styles.track); 
      })
    })
  }

  gettingThoseStats(region){
  
    let url =`data/stations.json`
    var self = this
    d3.json((url), (error, geodata) => {
      this.bigIncreases(geodata.usage)
       this.setState({
         stations: geodata.usage.sort(function(a, b){return b['1314 Entries & Exits'] - a['1314 Entries & Exits']}),
         allStations: geodata.usage,
         indicator: "1314 Entries & Exits"
       })
    })
  }

  showAll(){
    d3.selectAll('path').attr('opacity','1') 
    d3.selectAll('circle').attr('opacity','1') 
    d3.selectAll('circle').attr('fill','#fff').attr('r','6px').attr('stroke','none')
    const newStations = this.state.allStations.sort(function(a, b){return b['1314 Entries & Exits'] - a['1314 Entries & Exits']})
    this.setState({
      stations: newStations,
      indicator: "1314 Entries & Exits",
      identifier: null,
      showBusy:false
    })
  }

  makingAMap(region) {
  
    d3.select('#datamapcontainer').selectAll('svg').remove();
    const width = 700;
    const height = 600;
    const svg = d3.select('#datamapcontainer').append('svg')
    
    .attr('width', width)
    .attr('height', height)
    .attr('id', 'westYorkshire')

    const projection = d3.geoMercator()
    .scale(region.scale)
    .center(region.center) 
    .translate([width / 2, height / 2]); 
  
    const path = d3.geoPath()
    .projection(projection);
   
    const self = this;
    const locs = region.locations;
    const url = `data/combined.json`;

    d3.json((url), (error, geodata) => {
      const colours = ["cornflowerblue",'#304775','#4760b1','#3b6b84','steelblue']
      this.setState({
        path:path,
        projection:projection,
        svg:svg,
        geodata:geodata
      })
        geodata.regions.map(function(locadata,i){
          const identifier = Object.keys(locadata.objects)[0]
          const features = svg.append('g')
          .attr('class', 'features') 
          .attr('fill',colours[i])
          .attr('id',identifier)
          features.selectAll('path')
          .data(topojson.feature(locadata, locadata.objects[identifier]).features)
          .enter()
          .append('path')
          .attr('d', path)
          .attr('e', "what")
          .attr('class', d =>  identifier)
          .attr('id', d => d.properties.WD13NM.replace(/\s+/g, "").toLowerCase())
          .on('click', (e, i) => self.handleClick(e, identifier,projection,svg,i))
        })

       this.renderingStations(svg,projection,locs,path)
    });
  }

  handleClick(d,identifier,projection,svg) {

    const highlight = `#${d.properties.WD13NM.replace(/\s+/g, "").toLowerCase()}`
    const id = `.${identifier}`
    this.helperFunction(identifier)
    d3.selectAll('path').attr('opacity','0.4') 
    .attr("transform", d3.event.transform)
    .attr("fill", null)
    d3.selectAll(id)
    .attr("opacity", '1')
  }   

  handleStationClick(d,identifier,projection,svg) {

    const highlight = `#${d.properties.WD13NM.replace(/\s+/g, "").toLowerCase()}`
    const id = `.${identifier}`
    this.helperFunction(identifier)
    d3.selectAll('path').attr('opacity','0.4') 
    .attr("transform", d3.event.transform)
    .attr("fill", null)
    d3.selectAll(id)
    .attr("opacity", '1')
  }   

  render() {
    const centre = classNames(styles.module, styles.moduleShort);

    return (
      <div className={styles.mapView}>     
        <div className={styles.left}><div id="datamapcontainer">
          <section className={centre} />
        </div>
        <div>
          <Button identifier={this.state.identifier} showTotalsChart={this.showTotalsChart} showIncreaseChart={this.showIncreaseChart} allStations={this.state.allStations} showBusy={this.showBusy} showAll={this.showAll}/></div></div>
        <div className={styles.right}>
          <Charting chartName={this.state.chartName}  indicator={this.state.indicator} stations={this.state.stations}/>
        </div>
      </div>
    );
  }
}

export default Mapping
