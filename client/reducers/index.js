import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import region from './region.js';

const rootReducer = combineReducers({
  region,
  routing: routerReducer,
});

export default rootReducer;