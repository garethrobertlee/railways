const regions =


[{
	name: "Calderdale",
	topoJson: "E08000033",
  scale:73213.92708456448,
  center:[-1.9502583064355525,53.720609076279096],
	key:1,
},
{
	name: "Kirklees",
	topoJson: "E08000034",
  center:[-1.7850,53.6458],
  scale:63213.92708456448,
	key:2,
},
{
	name: "Bradford",
	topoJson: "E08000032",
  center:[-1.8600,53.8536],
  scale:58213.92708456448,
	key:3,
},
{
	name: "Leeds",
	topoJson: "E08000035",
  center:[-1.5491,53.8008],
  scale:30213.92708456448,
	key:4,
}
]

export default regions;