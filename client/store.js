import { createStore } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';

import rootReducer from './reducers/index';


import regions from './data/regions';



const defaultState = {

  region: regions[3],
};

const store = createStore(rootReducer, defaultState);


export const history = syncHistoryWithStore(browserHistory, store);

export default store;
