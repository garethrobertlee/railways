export default function changeRegion(event){
  return {
    type: "CHANGE_REGION",
    event,
  }
}
